# cоздаем цепочки
iptables -N ALLOW_ALL
iptables -N DB_SERVERS
iptables -N ON_DEMAND
iptables -N TEMP_ACCESS
iptables -N OUTGOING

# настраиваем цепочки
iptables -A ALLOW_ALL -j ACCEPT
iptables -A ALLOW_ALL -j LOG --log-prefix "[ALLOW_ALL] "

iptables -A DB_SERVERS -j ACCEPT
iptables -A DB_SERVERS -j LOG --log-prefix "[DB_SERVERS] "

iptables -A ON_DEMAND -j ACCEPT
iptables -A ON_DEMAND -j LOG --log-prefix "[ON_DEMAND] "

iptables -A TEMP_ACCESS -p tcp --match multiport --dports 80,443,22 -j ACCEPT
iptables -A TEMP_ACCESS -j LOG --log-prefix "[TEMP_ACCESS] "

iptables -A OUTGOING -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTGOING -j DROP
iptables -A OUTGOING -j LOG --log-prefix "[OUTGOING] "

read -p "Введите IP-адрес: " ip_address
echo "Вы ввели IP-адрес: $ip_address"
iptables -A INPUT -s 5.75.236.182 -j TEMP_ACCESS

iptables -P INPUT DROP
iptables -A INPUT -j LOG --log-prefix "[BLOCKED] "

# создаем файлы для логов
mkdir /var/log/iptables
touch /var/log/iptables/allow_all.log
touch /var/log/iptables/db_servers.log
touch /var/log/iptables/on_demand.log
touch /var/log/iptables/temp_access.log
touch /var/log/iptables/outgoing.log
touch /var/log/iptables/log_drop.log

# распределяем логи
touch /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[ALLOW_ALL] "'"' -/var/log/iptables/allow_all.log >> /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[DB_SERVERS] "'"' -/var/log/iptables/db_servers.log >> /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[ON_DEMAND] "'"' -/var/log/iptables/on_demand.log >> /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[TEMP_ACCESS] "'"' -/var/log/iptables/temp_access.log >> /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[OUTGOING] "'"' -/var/log/iptables/outgoing.log >> /etc/rsyslog.d/10-iptables.conf
echo :msg, contains, '"'"[BLOCKED] "'"' -/var/log/iptables/log_drop.log >> /etc/rsyslog.d/10-iptables.conf
service rsyslog restart

