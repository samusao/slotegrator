## Скрипт бекапа

```
pip3 install paramiko
```

## Использование

Запустите скрипт с флагом `-h` или `--help`, чтобы получить справку:

```bash
python3 backup.py -h
```

Запуск скрипта:

```
python3 backup.py
-h        show this help message and exit
-d DESTINATION, --destination DESTINATION
          Backup destination directory
-u USER, --user USER  
          SSH username
-i IP, --ip IP
          Remote server IP address
-p PRIVATE_KEY, --private-key PRIVATE_KEY
          Path to private key for SSH authentication
--debug               
          Enable debug mode
--source-dirs SOURCE_DIRS [SOURCE_DIRS ...]
          Source directories to backup
--max-backups MAX_BACKUPS [default: 2]
          Maximum number of backups to keep
--full                
          Perform a full backup
```

Пример

```
python3 backup.py -d <path_to_local_directory> -u admin -i <ip_server> --private-key <path_to_ssh_key> --source-dirs <path_to_remote_dirs>
```