import argparse
import os
import shutil
import datetime
import paramiko


# Сжимаем папку в .tar.gz, берем ее из remote_dir и сохраняем локально в local_dir
def create_backup(client, remote_dir, local_dir, backup_name, type_backup):
    if type_backup:
        remote_command = f"tar -cvzf" + remote_dir + ".tar.gz -C" + remote_dir + " ."
    else:
        remote_command = f"tar --verbose --create --listed-incremental=/etc/backup/snapshot.snar --level=0 --gzip --file=" + remote_dir + ".tar.gz -C" + remote_dir + " ."
    stdin, stdout, stderr = client.exec_command(remote_command)
    stdout.channel.recv_exit_status()
    ftp = client.open_sftp()
    remote_folder = os.path.join(remote_dir + ".tar.gz")
    local_item = os.path.join(local_dir + backup_name + ".tar.gz")
    ftp.get(remote_folder, local_item)
    ftp.close()
    print(f"Backup created: {backup_name}")


def rotate_backups(backup_dir, max_backups):
    backups = os.listdir(backup_dir)
    sorted_files = sorted(backups, key=lambda x: os.path.getctime(os.path.join(backup_dir, x)), reverse=True)
    if len(sorted_files) > max_backups:
        backups_to_move = sorted_files[max_backups:]
        for backup in backups_to_move:
            backup_path = os.path.join(backup_dir, backup)
            if os.path.isfile(backup_path):
                os.replace(backup_path, backup_dir[:-1] + "Old/" + backup)
            elif os.path.isdir(backup_path):
                shutil.rmtree(backup_path)
        print("Old backups rotated")


def main():
    parser = argparse.ArgumentParser(description="Backup script")
    parser.add_argument("-d", "--destination", required=True, help="Backup destination directory")
    parser.add_argument("-u", "--user", required=True, help="SSH username")
    parser.add_argument("-i", "--ip", required=True, help="Remote server IP address")
    parser.add_argument("-p", "--private-key", required=True, help="Path to private key for SSH authentication")
    parser.add_argument("--debug", action="store_true", help="Enable debug mode")
    parser.add_argument("--source-dirs", nargs="+", default=["/path/to/source"], help="Source directories to backup")
    parser.add_argument("--max-backups", type=int, default=2, help="Maximum number of backups to keep")
    parser.add_argument("--full", action="store_true", help="Perform a full backup")

    args = parser.parse_args()

    if args.debug:
        print("Debug mode enabled")

    current_datetime = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    if args.full:
        local_dir = args.destination + "/backup/full/"
    else:
        local_dir = args.destination + "/backup/inc/"

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if args.private_key:
        private_key = paramiko.RSAKey.from_private_key_file(args.private_key)
        client.connect(args.ip, username=args.user, pkey=private_key)

    os.makedirs(local_dir, exist_ok=True)
    os.makedirs(local_dir[:-1] + "Old/", exist_ok=True)

    for source_dir in args.source_dirs:
        original_path = source_dir
        path_to_name = original_path.replace("/", "-")
        backup_name = f"{args.ip}-{path_to_name}-{current_datetime}"
        create_backup(
            client=client,
            remote_dir=source_dir,
            local_dir=local_dir,
            backup_name=backup_name,
            type_backup=args.full
        )
        rotate_backups(local_dir, args.max_backups)

    client.close()


if __name__ == "__main__":
    main()
